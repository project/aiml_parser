
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

 * AIML Parser is a module which parses aiml tags and stores them
   in the database.

REQUIREMENTS
------------
No dependent module.

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module.

CONFIGURATION
-------------
 * Aiml Parser can be configured at admin/structure/aiml_parser.
   Here you can upload the aiml file which are needed to be parsed

MAINTAINER
-----------
Current maintainers:
 * Vipul Patil (vipul.patil7888) - https://www.drupal.org/u/vipul.patil7888
 * Nupur Lohokare (nupurlohokare) - https://www.drupal.org/u/nupurlohokare
 * Saurabh Tripathi (saurabh.tripathi.cs) - https://www.drupal.org/user/2436956/
