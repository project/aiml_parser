<?php

/**
 * @file
 * For declating entities.
 */

/**
 * Extend the defaults.
 */
/**
 * EntityDefaultMetadataController.
 */
class AimlParserTagsMetadataController extends EntityDefaultMetadataController {
  /**
   * Declaring entity properties.
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];
    $properties['fid'] = array(
      'label' => t('File Id'),
      'schema field' => 'fid',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('File identifier.'),
    );
    $properties['category'] = array(
      'label' => t('Category'),
      'schema field' => 'category',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('category tag of the aiml.'),
    );
    $properties['pattern'] = array(
      'label' => t('Pattern'),
      'schema field' => 'pattern',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('Pattern tag of the aiml.'),
    );
    $properties['template'] = array(
      'label' => t('Template'),
      'schema field' => 'template',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('Template tag of the aiml.'),
    );
    $properties['think'] = array(
      'label' => t('Think'),
      'schema field' => 'think',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('Think tag of the aiml.'),
    );
    $properties['set'] = array(
      'label' => t('Set'),
      'schema field' => 'set',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('Set tag of the aiml.'),
    );
    $properties['get'] = array(
      'label' => t('Get'),
      'schema field' => 'get',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('Get tag of the aiml.'),
    );
    $properties['star'] = array(
      'label' => t('Star'),
      'schema field' => 'star',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('Star tag of the aiml.'),
    );
    $properties['srai'] = array(
      'label' => t('Srai'),
      'schema field' => 'srai',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('Srai tag of the aiml.'),
    );
    $properties['random'] = array(
      'label' => t('Random'),
      'schema field' => 'random',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('Random tag of the aiml.'),
    );
    $properties['li'] = array(
      'label' => t('li'),
      'schema field' => 'li',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('li tag of the aiml.'),
    );
    $properties['that'] = array(
      'label' => t('That'),
      'schema field' => 'that',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('That tag of the aiml.'),
    );
    $properties['topic'] = array(
      'label' => t('Topic'),
      'schema field' => 'topic',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('Topic tag of the aiml.'),
    );
    $properties['condition'] = array(
      'label' => t('Condition'),
      'schema field' => 'condition',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('Condition tag of the aiml.'),
    );
    return $info;
  }

}
