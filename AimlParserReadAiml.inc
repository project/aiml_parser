<?php

/**
 * @file
 * Implemented AIML File Parser Logic.
 */

/**
 * This File Parses whole AIML.
 */
class AimlParserReadAiml {
  private $filecontent;
  /**
   * Loads AIML File Raw Data.
   */
  public function __construct($content) {
    $this->filecontent = $content;
  }

  /**
   * Get the content of AIML File.
   */
  public function readFile() {
    $raw_content = file_get_contents($this->filecontent->uri);
    return $raw_content;
  }

  /**
   * Returns the array of Category and Template Tag Values.
   */
  public function fileParser() {
    $raw_aiml = $this->readFile();
    $charset = "UTF-8";
    $valid_aiml_header = '<?xml version="1.0" encoding="[charset]"?>
       <!DOCTYPE aiml PUBLIC "-//W3C//DTD Specification Version 1.0//EN" "http://www.program-o.com/xml/aiml.dtd">
       <aiml version="1.0.1" xmlns="http://alicebot.org/2001/AIML-1.0.1">';
    $valid_aiml_header = str_replace('[charset]', $charset, $valid_aiml_header);
    $aiml_tag_start = stripos($raw_aiml, '<aiml', 0);
    $aiml_tag_end = strpos($raw_aiml, '>', $aiml_tag_start) + 1;
    $aiml_file = $valid_aiml_header . substr($raw_aiml, $aiml_tag_end);
    if (strpos($aiml_file, '</aiml>') == FALSE) {
      return t('An Empty AIML File OR No AIML tags Found.');
    }
    else {
      $xml = new DOMDocument();
      $xml->loadXML($aiml_file);
      return $this->parsePatternTemplateTag($xml);
    }
  }

  /**
   * Creates the Array from Raw Tag.
   */
  public function parsePatternTemplateTag(DOMDocument $xml) {
    $pattern_array = array();
    $pattern = $xml->getElementsByTagName('pattern');
    foreach ($pattern as $pat) {
      $pattern_array[] = $pat->nodeValue;
    }
    $template_array = array();
    $template = $xml->getElementsByTagName('template');
    foreach ($template as $temp) {
      $template_array[] = $temp->nodeValue;
    }
    $pat_temp = array_combine($pattern_array, $template_array);
    return $pat_temp;
  }

}
